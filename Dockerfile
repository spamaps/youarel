FROM python:3.6

RUN apt update && apt -y install libre2-dev build-essential
RUN pip install pipenv
COPY . /code
WORKDIR /code
RUN pipenv install
# Clean up all these build tools which we don't want in our runtime
RUN apt -y remove build-essential && apt -y autoremove && apt clean
CMD pipenv run python3 youarel/__init__.py
