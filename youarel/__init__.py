#!/usr/bin/env python3
# Copyright 2020 Clint Byrum
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import asyncio
import logging
import urllib
import argparse
import json

import re2
from aiohttp import web

logger = logging.getLogger()


def normalize_url(origin, path, query_string):
    """ Applies various methods to produce a single URL from various parts """
    origin = origin.lower()
    parsed_qs = urllib.parse.parse_qs(query_string)
    keys = [k for k in parsed_qs.keys()]
    keys.sort()
    new_qs = []
    for k in keys:
        new_qs.append(f"{k}={','.join(sorted(parsed_qs[k]))}")
    new_qs = "&".join(new_qs)
    return f"//{origin}{path}?{new_qs}"


async def urlinfo(request):
    (safe, reason) = request.app["youarel"].check_url(request)
    response = dict(safe=safe)
    if reason:
        response["reason"] = reason
    return web.Response(text=json.dumps(response), content_type="application/json")


class Youarel(object):
    malicious_urls = dict()
    origins = set()
    res = list()

    def load_config(self, path):
        config = json.loads(open(path).read())

        for url in config["malicious"]:
            parsed_url = urllib.parse.urlparse(url)
            self.malicious_urls[
                normalize_url(parsed_url.netloc, parsed_url.path, parsed_url.query)
            ] = True
        logger.info(f"Loaded {len(self.malicious_urls)} malicious urls from {path}")
        logger.debug(f"urls = {self.malicious_urls}")

        if "origin" in config:
            self.origins.update([x.casefold() for x in config["origin"]])
            logger.info(f"Loaded {len(self.origins)} malicious origins")
            logger.debug(f"origins = {self.origins}")

        if "re2" in config:
            for re in config["re2"]:
                self.res.append(re2.compile(re))
            logger.info(f"Loaded {len(self.res)} malicious regexps")

    def check_url(self, request):
        """ returns (safe: bool, reason: str) """
        logger.debug(f"Checking {request}")
        origin = request.match_info.get("origin")
        if origin is None:
            raise web.HTTPBadRequest(reason="missing origin")

        if origin.casefold() in self.origins:
            return (False, "ORIGIN")

        path = f"/{request.match_info.get('path', '')}"

        if request.query_string:
            qs = f"{request.query_string}"
        else:
            qs = ""

        normalized = normalize_url(origin, path, qs)
        if self.malicious_urls.get(normalized, False):
            result = (False, "MALICIOUS")
        else:
            # regexps, even with re2, are the most expensive so they go last
            for re in self.res:
                logger.debug(f"Checking {origin}{path}?{qs} against {re.pattern}")
                if re.match(f"{origin.casefold()}{path}?{qs}"):
                    logger.info(f"{request} matched on {re.pattern}")
                    result = (False, "PATTERN")
                    break
            else:
                result = (True, None)
        logger.info(
            f"Returning {result} for origin={origin},path={path},qs={qs},normalized={normalized}"
        )
        return result


def app_factory(path):
    app = web.Application()
    youarel = Youarel()
    youarel.load_config(path)
    app["youarel"] = youarel
    app.add_routes(
        [
            web.get("/urlinfo/1/{origin}/{path:[^{}]*}", urlinfo),
            web.get("/urlinfo/1/{origin}", urlinfo),
        ]
    )
    return app


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--urls-path", default="urls.json", help="Path to json urls configuration"
    )
    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG)
    logger.info("Starting YouArEl")

    app = app_factory(args.urls_path)
    web.run_app(app)


if __name__ == "__main__":
    sys.exit(main())
