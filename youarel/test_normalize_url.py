from youarel import normalize_url


def test_normalize_url():
    # basic
    assert normalize_url("test.com", "/", "") == "//test.com/?"
    upper_origin = normalize_url("TEST.COM", "/", "")
    lower_origin = normalize_url("test.com", "/", "")
    # same keys in diff order end up equal
    derp_first = normalize_url("test.com", "/a", "derp=disco&abba=disco")
    abba_first = normalize_url("test.com", "/a", "abba=disco&derp=disco")
    # same keys and values in diff order end up equal
    z_first = normalize_url("test.com", "/b/c", "foo=z&foo=a")
    a_first = normalize_url("test.com", "/b/c", "foo=a&foo=z")
    assert z_first == a_first
