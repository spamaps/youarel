import tempfile
import json
import logging
import uuid

import pytest

from youarel import app_factory
from aiohttp import web


TEST_URLS = {
    "malicious": [
        "https://test.com/malicious.html",
        "https://test.com/query?q=meep",
        "http://test.com/query?q1=athing&q2=2ndthing&q1=anotherthing",
    ],
    "origin": ["badstuff.test"],
    "re2": [
        "^nopath.test.*$",
        "^withpath.test/badpath.*$",
        "^qstring.test/.*\\?.*badvar=.*$",
        "^qstring.test/.*\\?.*=goodvar.*$",
    ],
}


@pytest.fixture
def runapp(loop, aiohttp_client, caplog):
    caplog.set_level(logging.DEBUG)
    with tempfile.NamedTemporaryFile() as tfile:
        tfile.write(json.dumps(TEST_URLS).encode("utf-8"))
        tfile.flush()
        app = app_factory(tfile.name)
    return loop.run_until_complete(aiohttp_client(app))


async def assert_response(resp, expected_body):
    assert resp.status == 200
    body = json.loads(await resp.text())
    assert body == expected_body


async def test_simple_match(runapp):
    """ match w/o query string """
    resp = await runapp.get("/urlinfo/1/test.com/malicious.html")
    await assert_response(resp, {"safe": False, "reason": "MALICIOUS"})


async def test_simple_nomatch(runapp):
    """ no match on any component """
    resp = await runapp.get("/urlinfo/1/test.org/safe.html")
    await assert_response(resp, {"safe": True})


async def test_match_query(runapp):
    """ matches a single query parameter """
    resp = await runapp.get("/urlinfo/1/test.com/query?q=meep")
    await assert_response(resp, {"safe": False, "reason": "MALICIOUS"})


async def test_match_query_multi(runapp):
    """ matches a multi-value query parameter in various orders """
    resp = await runapp.get(
        "/urlinfo/1/test.com/query?q2=2ndthing&q1=athing&q1=anotherthing"
    )
    await assert_response(resp, {"safe": False, "reason": "MALICIOUS"})


async def test_nomatch_query_multi(runapp):
    """ does not match if one value is not a match """
    resp = await runapp.get(
        "/urlinfo/1/test.com/query?q2=notamatch&q1=athing&q1=anotherthing"
    )
    await assert_response(resp, {"safe": True})


async def test_origin_match_all(runapp):
    """ matches known bad origins """
    resp = await runapp.get("/urlinfo/1/badstuff.test")
    await assert_response(resp, {"safe": False, "reason": "ORIGIN"})
    resp = await runapp.get("/urlinfo/1/badstuff.test/foo/bar")
    await assert_response(resp, {"safe": False, "reason": "ORIGIN"})
    test_uuid = uuid.uuid4()
    resp = await runapp.get("/urlinfo/1/badstuff.test/{test_uuid}")
    await assert_response(resp, {"safe": False, "reason": "ORIGIN"})
    resp = await runapp.get("/urlinfo/1/bAdsTufF.test/{test_uuid}")
    await assert_response(resp, {"safe": False, "reason": "ORIGIN"})


async def test_origin_nomatch(runapp):
    """ does not match on unknown origins """
    resp = await runapp.get("/urlinfo/1/goodstuff.test")
    await assert_response(resp, {"safe": True})
    resp = await runapp.get("/urlinfo/1/goodstuff.test/foo/bar")
    await assert_response(resp, {"safe": True})
    test_uuid = uuid.uuid4()
    resp = await runapp.get("/urlinfo/1/goodstuff.test/{test_uuid}")
    await assert_response(resp, {"safe": True})
    resp = await runapp.get("/urlinfo/1/gOodStUff.test/{test_uuid}")
    await assert_response(resp, {"safe": True})


async def test_re2_nopath(runapp):
    """ check that re2 matches work even if no path is in the regex """
    resp = await runapp.get("/urlinfo/1/nopath.test")
    await assert_response(resp, {"safe": False, "reason": "PATTERN"})
    resp = await runapp.get("/urlinfo/1/nopath.test/")
    await assert_response(resp, {"safe": False, "reason": "PATTERN"})
    resp = await runapp.get("/urlinfo/1/nopath.test?anyquery=foo")
    await assert_response(resp, {"safe": False, "reason": "PATTERN"})
    resp = await runapp.get("/urlinfo/1/nopath.test/?anyquery=foo")
    await assert_response(resp, {"safe": False, "reason": "PATTERN"})


async def test_re2_paths(runapp):
    """ check that re2 match paths as expected """
    resp = await runapp.get("/urlinfo/1/withpath.test")
    await assert_response(resp, {"safe": True})
    resp = await runapp.get("/urlinfo/1/withpath.test/goodpath")
    await assert_response(resp, {"safe": True})
    resp = await runapp.get("/urlinfo/1/withpath.test/goodpath?anyquery=foo")
    await assert_response(resp, {"safe": True})
    resp = await runapp.get("/urlinfo/1/withpath.test/badpath?anyquery=foo")
    await assert_response(resp, {"safe": False, "reason": "PATTERN"})


async def test_re2_querystring(runapp):
    """ check that re2 matches querystring stuff """
    resp = await runapp.get("/urlinfo/1/qstring.test")
    await assert_response(resp, {"safe": True})
    resp = await runapp.get("/urlinfo/1/qstring.test/somepath/?goodvar=foo")
    await assert_response(resp, {"safe": True})
    resp = await runapp.get("/urlinfo/1/qstring.test/?badvar=foo")
    await assert_response(resp, {"safe": False, "reason": "PATTERN"})
    resp = await runapp.get("/urlinfo/1/qstring.test/somepath?badvar=foo")
    await assert_response(resp, {"safe": False, "reason": "PATTERN"})
