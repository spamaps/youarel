# YouArEl

We have an HTTP proxy that is scanning traffic looking for malware URLs. Before
allowing HTTP connections to be made, this proxy asks a service that maintains
several databases of malware URLs if the resource being requested is known to
contain malware.

Write a small web service, in the language/framework your choice, that responds
to GET requests where the caller passes in a URL and the service responds with
some information about that URL. The GET requests look like this:

`GET /urlinfo/1/{hostname_and_port}/{original_path_and_query_string}`

The caller wants to know if it is safe to access that URL or not. As the
implementer, you get to choose the response format and structure. These lookups
are blocking users from accessing the URL until the caller receives a response
from your service.

# Config

The urls.json file included has examples of everything. It should be a mapping,
with two keys:

## malicious

This key should contain a list of exact-match URLs. Meaning if the origin,
path, and query are exactly the same as what is listed here, the URL will be
seen as unsafe, and the reason will be "MALICIOUS". Note that query string
arguments can be in different orders and still match. Origins are matched
case-insensitive.

## origin

A list of origins only to allow a blanket kill of an entire host. Matches to
this will return the reason "ORIGIN"

## re2

A list of [re2](https://github.com/google/re2/) regular expressions to apply to
urls submitted to urlinfo. These are more expensive than `malicious` or `re2`
as they require a "filter" approach where we apply all of them to every URL,
rather than hashed lookups, but they will allow the most power and may prove
useful for blocking known tools no matter the origin. Matches for these rules
will return a reason of "PATTERN".

# API

`GET /urlinfo/1/{hostname_and_port}/{original_path_and_query_string}`

If the URL matches the configured rules, the JSON mapping returned will have
the `safe` attribute set to `false`. Otherwise, it will be `true`. Any `false`
response will also include a `reason` attribute. For example:

```
{"safe": false, "reason": "MALICIOUS"}`
```

If the URL is not known to the system, you will receive this:

```
{"safe": true}
```

Both response codes should have a 200 code.

# Docker

Build the image with

`docker build -t youarel .`

Run it with

`docker run youarel`

The container will be listening on port 8080.

# Long term goals

* The size of the URL list could grow infinitely, how might you scale this
  beyond the memory capacity of this VM? Bonus if you implement this.

* The number of requests may exceed the capacity of this VM, how might you
  solve that?

* What are some strategies you might use to update the service with new URLs?
  Updates may be as much as 5 thousand URLs a day with updates arriving every
  10 minutes.

* Containerize the app.

# Development

* Code is formatted by [black](https://github.com/psf/black)
* Python 3.6+ required
* Run server with `pipenv run python3 youarel/__init__.py`
* Run tests with `pipenv run py.test`
